## Cara Run Program

- Install XAMPP terlebih dahulu [Install Xampp](https://sourceforge.net/projects/xampp/files/XAMPP%20Windows/8.2.4/xampp-windows-x64-8.2.4-0-VS16-installer.exe).
- Setelah itu clone project di terminal dengan cara **git clone https://gitlab.com/hanifanavianto2942/tugas-web-form.git** didalam folder **htdocs** atau download **ZIP** dan extract di dalam folder **htdocs**.
- Ubah isi file **index.php** di dalam folder **htdocs**, ubah link nya saja menjadi folder yang sudah di extract tadi.
- Jika sudah jalankan program dengan cara menyalakan apache terlebih dahulu di dalam **xampp control panel**, untuk mencari xampp control panel bisa dilakukan di pencarian windows.
- Jika sudah di nyalakan apache, lalu buka browser dan ketikan di bagian url **http://127.0.0.1** / **http://localhost**
- Setelah itu anda dapat menggunakan fitur kirim email yang dapat anda terima melalui email anda.
- Atau anda bisa lihat program di **https://hanifanavianto.000webhostapp.com/**
- Portfolio Hanifan Avianto **https://hanifanavianto.000webhostapp.com/**
## Author - Iban Syahdien Akbar

**About - Iban Syahdien Akbar**

Hi, Introduce me Iban Syahdan Akbar. I graduated from Gunadarma University, I majored in information systems, I learned about website-based programming and how to create structures that are used. Therefore, I am very interested in learning more about designing and building so that I can know what a web-based application needs sequentially.

To solve problems in such web-based applications, I have experience in creating and designing through several projects, all done by defining various stakeholder requirements, conducting user interviews so I can get their insights, learn from them, and turn them into ideas. . which will provide a better experience for them.

Apart from learning about Website-Based Programming, I don't limit myself to learning other things related to my major such as analysis and design of information systems and database systems.

I can handle working with different types of people to create new experiences and learn new perspectives, I can work in a team or individually, I also want to learn more about the work I will be working on in the future.

## Portfolio - Iban Syahdien Akbar
You can see Iban Syahdien Akbar portfolio through the following website: **https://ibansyah.pesanin.com/**

## Project - Iban Syahdien Akbar
- PT. Indah Logistik - [View](https://indahonline.com/)
- SMK Kesuma Bangsa - [View](https://smk-kesumabangsa1.sch.id/)
- PT. Trans Properti Manajemen - [View](https://tpm.co.id/)
- PT. Trimulken Rayagung - [View](https://trimulkenrayagung.co.id/)
